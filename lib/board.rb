class Board

  attr_accessor :grid

  def initialize(grid = nil)
    if grid == nil
      @grid = [[nil, nil, nil],
      [nil, nil, nil],
      [nil, nil, nil]]
    else
      @grid = grid
    end
  end

  def place_mark(pos, mark)
    x = pos[0]
    y = pos[1]
    if empty?(pos)
      grid[x][y] = mark
    else
      raise "invalid move"
    end
  end

  def empty?(pos)
    x = pos[0]
    y = pos[1]
    if grid[x][y]
      return false
    end
    true
  end

  def get_value(pos)
    return grid[pos[0]][pos[1]]
  end

  def winner
    winning_positions = [
    [[0, 0], [0, 1], [0, 2]],
    [[1, 0], [1, 1], [1, 2]],
    [[2, 0], [2, 1], [2, 2]],
    [[0, 0], [1, 0], [2, 0]],
    [[0, 1], [1, 1], [2, 1]],
    [[0, 2], [1, 2], [2, 2]],
    [[0, 0], [1, 1], [2, 2]],
    [[0, 2], [1, 1], [2, 0]]
    ]
    winning_positions.each do |arr1|
      positions = arr1.map { |arr2| get_value(arr2) }
      if positions.uniq.length == 1 && positions.uniq != [nil]
        return positions[0]
      end
    end
    nil
  end

  def over?
    grid.each do |arr|
      if arr.include?(nil)
        if self.winner == nil
          return false
        end
      end
    end
    true
  end

end
