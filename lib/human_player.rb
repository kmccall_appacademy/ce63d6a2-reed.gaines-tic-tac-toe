class HumanPlayer

  attr_accessor :mark, :name

  def initialize(name)
    @name = name
    @mark = :X
  end

  def get_move
    puts "Where do you want to mark?"
    puts "(Format: X, Y)"
    $stdout.flush
    move = gets
    [move.chars.first.to_i, move.chars.last.to_i]
  end

  def display(board)
    puts board.grid
  end

end
