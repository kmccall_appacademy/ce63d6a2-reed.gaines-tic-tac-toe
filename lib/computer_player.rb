class ComputerPlayer

  attr_accessor :mark, :name, :board

  def initialize(name)
    @name = name
    @mark = :O
  end

  def display(board)
    @board = board
  end

  def get_move
    grid = board.grid
    winning_positions = [
    [[0, 0], [0, 1], [0, 2]],
    [[1, 0], [1, 1], [1, 2]],
    [[2, 0], [2, 1], [2, 2]],
    [[0, 0], [1, 0], [2, 0]],
    [[0, 1], [1, 1], [2, 1]],
    [[0, 2], [1, 2], [2, 2]],
    [[0, 0], [1, 1], [2, 2]],
    [[0, 2], [1, 1], [2, 0]]
    ]
    random_nums = [0, 1, 2]

    winning_positions.each do |arr1|
      positions = arr1.map { |arr2| board.get_value(arr2) }
      if positions.count(:O) == 2 && positions.include?(nil)
        winning_row = arr1
        winning_row.each do |pos|
          if board.get_value(pos) == nil
            return pos
          end
        end
      end
    end

    [random_nums.shuffle[0], random_nums.shuffle[0]]

  end

end
