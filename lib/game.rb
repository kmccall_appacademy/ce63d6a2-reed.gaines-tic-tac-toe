require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :player_one, :player_two, :board, :current_player, :counter

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = player_one
  end

  def play_turn
    current_player.display
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
  end

  def switch_players!
    counter = 1

    if counter.odd?
      current_player = player_two
    else
      current_player = player_one
    end
    
    counter += 1
  end

  def play
    until board.over?
      play_turn
    end

      puts "#{current_player.name} won!"
  end
end
